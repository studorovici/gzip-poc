package com.deloitte.gzippoc.controller;

import com.deloitte.gzippoc.services.GzipService;
import com.deloitte.gzippoc.services.StandardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    private final GzipService gzipService;
    private final StandardService standardService;

    @Autowired
    public Controller(GzipService gzipService, StandardService standardService) {
        this.gzipService = gzipService;
        this.standardService = standardService;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value = "/gzipped",
            produces = "application/json"
    )
    @ResponseBody
    public ResponseEntity getGzipped() {
        try {
            return ResponseEntity.ok(gzipService.getJson());
        } catch (Exception e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    @GetMapping("/standard")
    public ResponseEntity<String> getStandard() {
        try {
            return ResponseEntity.ok(standardService.getJsonAsString());
        } catch (Exception e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }
}
