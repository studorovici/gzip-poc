package com.deloitte.gzippoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GzipPocApplication {

    public static void main(String[] args) {
        SpringApplication.run(GzipPocApplication.class, args);
    }

}
