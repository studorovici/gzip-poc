package com.deloitte.gzippoc.services;

import com.google.common.io.Resources;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@Service
public class StandardService {

    public String getJsonAsString() throws IOException {
        URL resource = Resources.getResource("json/product.json");
        String content = Resources.toString(resource, StandardCharsets.UTF_8);

        return content;
    }
}
