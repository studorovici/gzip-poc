package com.deloitte.gzippoc.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;

@Service
public class GzipService {

    private final ObjectMapper mapper = new ObjectMapper();

    public JsonNode getJson() throws IOException {
        URL resource = Resources.getResource("json/product.json");
        JsonNode node = mapper.readTree(resource);

        return node;
    }
}
