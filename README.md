##Gziping requests POC

Steps to launch:
1. Open in IntelliJ and launch main method
2. Open postman and call the 2 endpoints:
    - http://localhost:8080/standard
    - http://localhost:8080/gzipped
3. Compare results

Standard behavior (returning a _String_):<br />
![Standard behavior](/img/standard.png)

Gzipped behavior (returning a _gzipped json_):<br />
![Gzipped behavior](/img/gzipped.png)


I used a dummy product containing **57274** lines